### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://github.com/mpv-player/mpv.git
cd mpv

#Z#
apt build-dep -y mpv
apt install -y python

./bootstrap.py
./waf configure

./waf

```

### QTCreator Includes
```

```
